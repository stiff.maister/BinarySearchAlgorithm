import java.util.Arrays;

/**
 * Created by Andrea on 13/05/2018.
 */
public class MergeSort {

	public static void main(String[] args) {
		int[] arr = {5, 4, 7, 2, 3, 1, 6, 2};

		System.out.println(Arrays.toString(arr));
		new MergeSort().sort(arr, 0, arr.length - 1);
		System.out.println(Arrays.toString(arr));

	}


	private void sort(int[] array, int lowerIndex, int higherIndex) {
		if (lowerIndex < higherIndex) {
			int middle = (lowerIndex + higherIndex) / 2;   // find the middle index value to split the array
			sort(array, lowerIndex, middle);               // recursive call to divide the sub-list
			sort(array, middle + 1, higherIndex); // recursive call to divide the sub-list
			merge(array, lowerIndex, middle, higherIndex);

		}
	}


	private void merge(int[] array, int lowerIndex, int middle, int higherIndex) {
		// allocate enough space so that the extra 'sentinel' value
		// can be added. Each of the 'left' and 'right' sub-lists are pre-sorted.
		// This function only merges them into a sorted list.
		int[] left = new int[(middle - lowerIndex) + 2];
		int[] right = new int[higherIndex - middle + 1];

		// create the left and right sub-list for merging into original list.
		System.arraycopy(array, lowerIndex, left, 0, left.length - 1);
		System.arraycopy(array, middle + 1, right, 0, left.length - 1);


		// giving a sentinel value to marking the end of the sub-list.
		// Note: The list to be sorted is assumed to contain numbers less than 100.
		left[left.length - 1] = 100;
		right[right.length - 1] = 100;

		int i = 0;
		int j = 0;

		// loop to merge the sorted sequence from the 2 sub-lists(left and right)
		// into the main list.
		for (; lowerIndex <= higherIndex; lowerIndex++) {
			if (left[i] <= right[j]) {
				array[lowerIndex] = left[i];
				i++;
			} else {
				array[lowerIndex] = right[j];
				j++;
			}
		}
	}
}