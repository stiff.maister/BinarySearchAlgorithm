/**
 * Created by Andrea on 28/04/2018.
 */

fun main(args: Array<String>) {
    fun printBetween(array: Array<Int>, low: Int, high: Int) {
        print("Array is: ")
        for (i in low..high) print("" + array[i] + " ")
        println()
    }

    /**
     * Funzione che effettua una binary search su un array ordinato di numeri
     */
    fun binarySearch(sortedArray: Array<Int>, target: Int): Int {
        var runCounter = 0
        var found = false
        var high = sortedArray.size - 1 // indice massimo superiore
        var low = 0                // indice minimo inferiore
        var index = 0
        println("Target is: $target")

        while (low <= high) {
            var mid = low + (high - low) / 2 // vado a calcolare l'indice della metà dell'array
            println("RUN ---------> $runCounter")
            println("Low: $low, High: $high, Mid: $mid")


            if (sortedArray[mid] < target) { // se il valore dell'elemento a metà dell'array è inferiore a quello del target
                low = mid + 1               // vado a selezionare la metà destra dell'array
                println("Selecting the right-half part of the array")

            } else if (sortedArray[mid] > target) { // altrimenti se il valore è maggiore di quello del target seleziono
                high = mid - 1                      // la parte sinistra dell'array
                println("Selecting the left-half part of the array")

            } else if (sortedArray[mid] == target) { // ho trovato il valore e quindi ritorno la posizione
                index = mid
                println("Target found at index: $index")
                found = true
                break
            }
            printBetween(sortedArray, low, high)

            runCounter++

        }
        if (found) return index
        else throw error("Target is not present in the provided sorted array")
    }


    var sortedArray: ArrayList<Int> = ArrayList()
    for (i in 1..800) sortedArray.add(i)

    print("Element found at position: " + binarySearch(sortedArray.toTypedArray(), 6))
}