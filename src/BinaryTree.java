import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Andrea on 05/05/2018.
 */
public class BinaryTree {

	Node root;


	public void add(int value) {
		root = addRecursive(root, value);
	}

	public boolean contains(int value) {
		return containsNodeRecursive(root, value);
	}

	public void delete(int value) {
		deleteRecursive(root, value);
	}

	/**
	 * this method will transverse the tree in-order
	 */
	public void traverseInOrder(Node node) {
		if (node != null) {
			traverseInOrder(node.left);
			System.out.print(" " + node.value);
			traverseInOrder(node.right);
		}
	}

	/**
	 * Pre-order traversal visits first the root node, then the left subtree, and finally the right subtree.
	 */
	public void traversePreOrder(Node node) {
		if (node != null) {
			System.out.print(" " + node.value);
			traversePreOrder(node.left);
			traversePreOrder(node.right);
		}
	}


	/**
	 * Post-order traversal visits the left subtree, the right subtree, and the root node at the end.
	 */
	public void traversePostOrder(Node node) {
		if (node != null) {
			traversePostOrder(node.left);
			traversePostOrder(node.right);
			System.out.print(" " + node.value);
		}
	}

	/**
	 * Breadth-First Search visits all the nodes of a level before going to the next level. This kind of traversal is
	 * also called level-order and visits all the levels of the tree starting from the root, and from left to right.
	 * For the implementation, we’ll use a Queue to hold the nodes from each level in order. We’ll extract each node
	 * from the list, print its values, then add its children to the queue.
	 */
	public void traverseLevelOrder() {
		if (root == null) {
			return;
		}

		Queue<Node> nodes = new LinkedList<>();
		nodes.add(root);

		while (!nodes.isEmpty()) {

			Node node = nodes.remove();

			System.out.print(" " + node.value);

			if (node.left != null) {
				nodes.add(node.left);
			}

			if (node.right != null) {
				nodes.add(node.right);
			}
		}
	}

	private Node addRecursive(Node current, int value) {
		// if the node is null we reached the leaf so we can insert the value in the leaf
		if (current == null) {
			return new Node(value);
		}
		// if the new value is lower than the current value we have to insert it on the left
		if (value < current.value) {
			current.left = addRecursive(current.left, value);
		} else if (value > current.value) { // if the new value is higher than the current value we have to insert it on the right
			current.right = addRecursive(current.right, value);
		} else {
			// value already exists
			return current;
		}
		return current;
	}

	private boolean containsNodeRecursive(Node current, int value) {
		// reached the end without finding value
		if (current == null) {
			return false;
		}

		// we have found the value
		if (value == current.value) {
			return true;
		}

		return value < current.value ?
				containsNodeRecursive(current.left, value) :
				containsNodeRecursive(current.right, value);


	}

	/**
	 * Once we find the node to delete, there are 3 main different cases:
	 * <p>
	 * a node has no children – this is the simplest case; we just need to replace this node with null in its parent node
	 * a node has exactly one child – in the parent node, we replace this node with its only child.
	 * a node has two children – this is the most complex case because it requires a tree reorganization
	 */
	private Node deleteRecursive(Node current, int value) {
		if (current == null) {
			return null;
		}

		if (value == current.value) {
			// first case
			if (current.left == null && current.right == null) {
				return null;
			}

			// second case
			if (current.right == null) {
				return current.left;
			}
			if (current.left == null) {
				return current.right;
			}

			int smallestValue = findSmallestValue(current.right);
			current.value = smallestValue;
			current.right = deleteRecursive(current.right, smallestValue);
			return current;
		}
		if (value < current.value) {
			current.left = deleteRecursive(current.left, value);
			return current;
		}
		current.right = deleteRecursive(current.right, value);
		return current;
	}

	private int findSmallestValue(Node root) {
		return root.left == null ? root.value : findSmallestValue(root.left);
	}

	class Node {
		int value;
		Node left;
		Node right;

		public Node(int value) {
			this.value = value;
			this.left = null;
			this.right = null;
		}
	}


	public static void main(String[] args) {

		BinaryTree binaryTree = new BinaryTree();
		binaryTree.add(6);
		binaryTree.add(4);
		binaryTree.add(8);
		binaryTree.add(3);
		binaryTree.add(5);
		binaryTree.add(7);
		binaryTree.add(9);

		System.out.println(binaryTree.contains(15));

		System.out.println("Transverse in order");
		binaryTree.traverseInOrder(binaryTree.root);
		System.out.println("\nTransverse Pre order, first the root, then left, than right");
		binaryTree.traversePreOrder(binaryTree.root);
		System.out.println("\nTransverse Post order, first left, then right, then root");
		binaryTree.traversePostOrder(binaryTree.root);
		System.out.println("\nTransverse in lever order");
		binaryTree.traverseLevelOrder();

	}
}
