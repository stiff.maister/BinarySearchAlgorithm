import java.util.Arrays;

/**
 * Insertion sort is a simple sorting algorithm, it builds the final sorted array one item at a time. It is much less
 * efficient on large lists than other sort algorithms.
 * <p>
 * Advantages of Insertion Sort:
 * 1) It is very simple.
 * 2) It is very efficient for small data sets.
 * 3) It is stable; i.e., it does not change the relative order of elements with equal keys.
 * 4) In-place; i.e., only requires a constant amount O(1) of additional memory space.
 * <p>
 * Insertion sort iterates through the list by consuming one input element at each repetition, and growing a sorted
 * output list. On a repetition, insertion sort removes one element from the input data, finds the location it belongs
 * within the sorted list, and inserts it there. It repeats until no input elements remain.
 * The best case input is an array that is already sorted. In this case insertion sort has a linear running time (i.e., Θ(n)).
 * During each iteration, the first remaining element of the input is only compared with the right-most element of the sorted
 * subsection of the array. The simplest worst case input is an array sorted in reverse order. The set of all worst case inputs
 * consists of all arrays where each element is the smallest or second-smallest of the elements before it. In these cases
 * every iteration of the inner loop will scan and shift the entire sorted subsection of the array before inserting the next element.
 * This gives insertion sort a quadratic running time (i.e., O(n2)). The average case is also quadratic, which makes insertion
 * sort impractical for sorting large arrays. However, insertion sort is one of the fastest algorithms for sorting very
 * small arrays, even faster than quicksort; indeed, good quicksort implementations use insertion sort for arrays smaller
 * than a certain threshold, also when arising as subproblems; the exact threshold must be determined experimentally and
 * depends on the machine, but is commonly around ten.
 * <p>
 * Array is imaginary divided into two parts - sorted one and unsorted one. At the beginning, sorted part contains first
 * element of the array and unsorted one contains the rest. At every step, algorithm takes first element in the unsorted
 * part and inserts it to the right place of the sorted one. When unsorted part becomes empty, algorithm stops.
 */
public class InsertionSort {

	public static void main(String[] args) {

		int[] array = {4, 1, 3, 2, 9};
		System.out.println("Sorted array: " + Arrays.toString(sort(array)));
	}


	public static int[] sort(int[] inputArray) {
		int i, j, newValue;
		// si inizia da 1 perchè si salta il primo elemento dell'inputArray in quanto questo viene considerato il primo inserimento del sortedArray
		for (i = 1; i < inputArray.length; i++) {
			newValue = inputArray[i]; // prendo il valore dell'elemento dell'array ad indice i
			j = i; // mi salvo il valore dell'indice in una variabile

			// sezione dell'algoritmo in cui viene fatto lo scorrimento della porzione ordinata dell'array. Controllo che l'indice j che tiene il conto
			// dello scorrimento non sia arrivato a zero e che il valore precedente a quello che sto analizzando è maggiore. Se è così
			while (j > 0 && inputArray[j - 1] > newValue) {
				inputArray[j] = inputArray[j - 1]; // sposto l'elemento precedente nella posizione dopo (faccio scorrere verso destra)
				j--; // diminuisco il contatore così da far scorrere verso destra finchè non viene trovata la posizione corretta per newValue
			}
			inputArray[j] = newValue; // ho trovato la posizione corretta per new value
		}

		return inputArray;
	}

}
