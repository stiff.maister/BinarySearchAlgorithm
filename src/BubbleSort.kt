import java.util.*

/**
 * This is one of the most straightforward sorting algorithms. The core idea is to keep swapping adjacent elements of
 * an array if they are in an incorrect order until the collection is sorted. As sorting is performed by swapping, we
 * can say it performs in-place sorting. Also, if two elements have same values, resulting data will have their order
 * preserved – which makes it a stable sort. This algorithm works in n-1 pass where n is the number of elements to sort
 * (The bubble sort algorithm can be easily optimized by observing that the n-th pass finds the n-th largest element and
 * puts it into its final place. So, the inner loop can avoid looking at the last n − 1 items when running for the n-th time).
 * After the first run (pass 0) we will see that the largest value is at the end of the array. On the next run (pass 1)
 * the second largest element will be at the second-last position, and so on until the (n-1)th run where the smallest
 * element will be already in the first position. Every pass contains n-1 steps where all the array is scanned and
 * sorting is done. The time complexity of the algorithm is O(n²), that means the time needed to run the algorithm grows
 * as square of number of elements grows. The memory complexity is O(1) that means the algorithm will not take any extra
 * memory except for the constants.
 */
fun main(args: Array<String>) {
    var array: IntArray = intArrayOf(2, 5, 6, 2, 1);
    var sortedArray: IntArray = BubbleSort.sort(array);
    println("Sorted array: ${Arrays.toString(sortedArray)}");

}

class BubbleSort {

    companion object {
        fun sort(array: IntArray): IntArray {
            println("Initial numbers: [%s]".format(array.joinToString(separator = ", ")))

            for (pass in 0 until (array.size - 1)) {
                // this is a single pass
                for (currentPosition in 0 until (array.size - pass - 1)) { // correction for optimization. At each pass the pass-th element will be already ordered and further comparison will have no benefit
                    print("Pass-%d-Step-%d: Comparing elements at position %d(%d) and %d(%d). ".format(pass, currentPosition, currentPosition, array[currentPosition], (currentPosition + 1), array[currentPosition + 1]))
                    // this is a single step
                    if (array[currentPosition] > array[currentPosition + 1]) {
                        println("They are in wrong order, swap them")
                        val temp = array[currentPosition];
                        array[currentPosition] = array[currentPosition + 1];
                        array[currentPosition + 1] = temp;
                    } else {
                        println("They are in correct order, do not swap them")

                    }
                    println("Numbers after Pass-%d-Step-%d: [%s]".format(pass, currentPosition, array.joinToString(separator =
                    ", ")))
                }
            }
            return array;
        }
    }
}
