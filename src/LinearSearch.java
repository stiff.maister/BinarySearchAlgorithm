/**
 * Created by Andrea on 13/05/2018.
 */
public class LinearSearch {

	public static void main(String[] args) {
		int[] array = {5, 2, 0, 1, 7, 9, 4, 1, 5};
		int position = linearSearch(array, 1);

		if (position == -1){
			System.out.println("Target not found");
		} else {
			System.out.println("Target found at position: " + position);
		}
	}


	public static int linearSearch(int[] array, int target){
		for (int i = 0; i < array.length; i++) {
			if (array[i] == target)
				return i;
		}
		return -1;
	}
}
