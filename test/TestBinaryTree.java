import org.junit.Assert;
import org.junit.Test;

public class TestBinaryTree {


	@Test
	public void createBinaryTreeAndSearchElement() {
		BinaryTree binaryTree = createBinaryTree();
		Assert.assertTrue(binaryTree.contains(3));
		Assert.assertFalse(binaryTree.contains(1000));
	}

	@Test
	public void createBinaryTreeAndDeleteElement() {
		BinaryTree binaryTree = createBinaryTree();
		Assert.assertTrue(binaryTree.contains(9));
		binaryTree.delete(9);
		Assert.assertFalse(binaryTree.contains(9));
	}

	private BinaryTree createBinaryTree() {
		BinaryTree binaryTree = new BinaryTree();
		binaryTree.add(6);
		binaryTree.add(4);
		binaryTree.add(8);
		binaryTree.add(3);
		binaryTree.add(5);
		binaryTree.add(7);
		binaryTree.add(9);

		return binaryTree;
	}
}
